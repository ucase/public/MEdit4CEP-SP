# MEdit4CEP-SP

MEdit4CEP-SP is an extension of the MEdit4CEP approach [[1](https://ucase.uca.es/medit4cep/)] which is the result of integrating the SP architecture [[2](https://gitlab.com/ucase/public/sp-architecture)] with MEdit4CEP. The former allows us to consume, process, and analyse huge amounts of heterogeneous information in real time, while the latter allows us to graphically and intuitively define and model CEP domains and event patterns to be detected.

## Installation

The steps to install MEdit4CEP-SP are quite similar to those required in the MEdit4CEP's installation. These are as follow:

1. Download and install Eclipse Epsilon from [[3](https://www.eclipse.org/epsilon/download/)]. It is highly recommended to use the Eclipse installer, if so, choose Epsilon in the installer.
2. Go to the installation folder and start Eclipse Epsilon.
3. You may be asked for a folder as a workspace, this folder is the one used to store the projects.
4. Select File > Import > Git > Projects from Git, press Next, select URI and introduce https://gitlab.com/ucase/public/MEdit4CEP-SP.git as Git repository.
5. Click Next and select the folder where the repository will be stored in your local machine.
6. Finally, in the wizard, select the import option: 'Import existing Eclipse projects'.

_Note for Windows users: please make sure that you extract the install Eclipse Epsilon close to the root of a drive (e.g. C:\) as the maximum path length on Windows may not exceed 256 characters._

## Usage

Once the repository is cloned, you can execute the MEdit4CEP-SP editor clicking on Run > Run Configurations..., double-click Eclipse Application, set a name, and press Apply and Run. If Eclipse warns you about existing errors ignore them, some of these errors are bugs of Eclipse Epsilon itself. This will execute an instance of MEdit4CEP-SP. Remember that you need the API [[4](https://gitlab.com/ucase/public/API-MEdit4CEP-SP)] and the SP architecture [[2](https://gitlab.com/ucase/public/sp-architecture)] to be running at the same time.

In order to create new CEP domains and CEP Event Patterns, please check the official documentation available at [[1](https://ucase.uca.es/medit4cep/)].


