/*******************************************************************************
 * Copyright (c) 2019 David Corral-Plaza
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0 
 * which accompanies this distribution, and is available at 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Corral-Plaza  - modifications marked as @generated NOT
 ******************************************************************************/
package domain.diagram.part;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.epsilon.egl.util.FileUtil;
import org.eclipse.gmf.tooling.runtime.actions.DefaultDeleteElementAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import domain.CEPDomain;
import domain.diagram.status.DomainStatus;
import es.uca.modeling.cep.utils.*;
import eventpattern.CEPEventPattern;
import eventpattern.diagram.status.EventPatternsStatus;
import eventpattern.impl.EventImpl;
import eventpattern.impl.EventPropertyImpl;
import okhttp3.Response;

/**
 * @generated NOT
 */
public class DeleteElementAction extends DefaultDeleteElementAction {

	/**
	 * @generated
	 */
	public DeleteElementAction(IWorkbenchPart part) {
		super(part);
	}

	Shell shell = Display.getCurrent().getActiveShell();

	@Override
	public void runWithEvent(Event event) {
		System.out.println("****** Deleting with mouse");

		if (MessageDialog.openConfirm(shell, "Delete Event Type",
				"Are you sure that you want to permanently delete the event type? "
						+ "This action will remove all the patterns that depend on this event "
						+ "type from the CEP engine and the API, but not from MEdit4CEP-SP, so you can edit, save and deploy them again.")) {
			super.runWithEvent(event);
			updatePatterns(removeEvent(DomainStatus.getActiveEvent()));
		} else {
			System.out.println("Simple Event Type not removed");
		}
	}

	@Override
	public void run() {
		System.out.println("****** Deleting with keyboard");
		if (MessageDialog.openConfirm(shell, "Delete Event Type",
				"Are you sure that you want to permanently delete the event type? "
						+ "This action will remove all the patterns that depend on this event "
						+ "type from the CEP engine and the API, but not from MEdit4CEP-SP, so you can edit, save and deploy them again.")) {
			super.run();
			updatePatterns(removeEvent(DomainStatus.getActiveEvent()));
		} else {
			System.out.println("Simple Event Type not removed");
		}
	}

	protected void updatePatterns(ArrayList<String> patternsToRemove) {
		String domainName = EventPatternsStatus.getDomainName();
		IWorkspaceRoot myWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		IProject patternProject = myWorkspaceRoot.getProject(domainName + "_patterns");
		IProject complexEventProject = myWorkspaceRoot.getProject(domainName + "_complex_events");
		ResourceSet resourceSet = new ResourceSetImpl();

		// Clean affected patterns
		for (int i = 0; i < patternsToRemove.size() - 1; i++) {
			String patternToRemove = patternsToRemove.get(i);
			System.out.println("Cleaning " + patternToRemove);
			URI activePatternModelUriAux = URI.createPlatformResourceURI(
					patternProject.getFile(patternToRemove + ".pattern").getFullPath().toString(), false);
			Resource patternModelResourceAux = resourceSet.getResource(activePatternModelUriAux, true);
			CEPEventPattern eventPatternModelAux = (CEPEventPattern) patternModelResourceAux.getContents().get(0);

			// Set pattern MongoDB ID to null
			eventPatternModelAux.setMongodbId(null);
			ArrayList<EObject> elementsToRemove = new ArrayList<>();
			for (TreeIterator iterator = EcoreUtil.getAllContents(eventPatternModelAux, true); iterator.hasNext();) {
				EObject eObjt = (EObject) iterator.next();

				if (eObjt.getClass().getSimpleName().equals("EventImpl")) {
					eventpattern.impl.EventImpl Event = (EventImpl) eObjt;
					if (patternsToRemove.contains(Event.getTypeName())) {
						elementsToRemove.addAll(Event.getOutboundLink());
						elementsToRemove.add(Event);

						// Remove event properties and their links
						for (TreeIterator iterAux = eObjt.eAllContents(); iterAux.hasNext();) {
							EObject eObjectAux = (EObject) iterAux.next();

							if (eObjectAux.getClass().getSimpleName().equals("EventPropertyImpl")) {
								eventpattern.impl.EventPropertyImpl EventProperty = (EventPropertyImpl) eObjectAux;

								elementsToRemove.add(EventProperty);
								elementsToRemove.addAll(EventProperty.getOutboundLink());
							}
						}
					}
				}
			}

			EcoreUtil.removeAll(elementsToRemove);

			try {
				patternModelResourceAux.save(null);
			} catch (Exception e1) {
				System.out.println("Exception: " + e1.getMessage());
			}

			DomainStatus.setDomainModified(true);
		}

		// Clean affected complex events domain diagram
		IFile complexEventFile = complexEventProject.getFile(domainName + "_complex_events" + ".domain_diagram");
		URI complexEventModelUri = URI.createPlatformResourceURI(
				complexEventProject.getFile(domainName + "_complex_events" + ".domain").getFullPath().toString(),
				false);
		Resource complexEventModelResource = resourceSet.getResource(complexEventModelUri, true);
		CEPDomain domainModel = (CEPDomain) complexEventModelResource.getContents().get(0);
		
		ArrayList<domain.Event> complexEventsToRemove = new ArrayList<>();
		for (TreeIterator iter = EcoreUtil.getAllContents(domainModel, true); iter.hasNext();) {
			EObject eObject = (EObject) iter.next();

			if (eObject.getClass().getSimpleName().equals("EventImpl")) {
				domain.Event domainEvent = (domain.Event) eObject;
				if (patternsToRemove.contains(domainEvent.getTypeName())) {
					complexEventsToRemove.add(domainEvent);
				}
			}
		}

		EcoreUtil.removeAll(complexEventsToRemove);
		try {
			complexEventModelResource.save(null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected ArrayList<String> removeEvent(domain.Event event) {
		if (event != null) {
			System.out.println("Removing Simple Event: " + event.toString());

			if (event.getMongodbId() != null) {
				Response response = HTTPRequest
						.DELETE("/eventTypes/" + event.getMongodbId() + "/" + EventPatternsStatus.getDomainName());

				if (response.code() == 500)
					MessageDialog.openError(shell, "Delete Event Type",
							"There was an error while removing the domain in the API. Please try again.");
				else if (response.code() == 404)
					MessageDialog.openError(shell, "Delete Event Type",
							"The event domain's was not found in the API. Please check it and try again.");
				else if (response.code() == 200) {
					String jsonString;

					try {
						jsonString = response.body().string();
					} catch (IOException e) {
						e.printStackTrace();
						return new ArrayList<String>();
					}

					Gson gson = new Gson();
					JsonObject jsonRes = gson.fromJson(jsonString, JsonObject.class);
					response.close();

					if (MessageDialog.openConfirm(shell, "Backup Patterns",
							"Would you like to backup the patterns that are going to be removed?"))
						savePatterns(jsonRes.get("patterns").getAsJsonArray(), shell);

					if (KafkaCEP.send("streams-undeploy", jsonRes.get("deploymentId").getAsString()))
						MessageDialog.openInformation(shell, "Delete Event Type",
								"The event type has been deleted from the API and undeployed from CEP engine.");
					else
						MessageDialog.openError(shell, "Delete Event Type",
								"There was an error undeploying the event type form the CEP engine.");

					return new ArrayList<String>(Arrays.asList(jsonRes.get("patternsToRemove").getAsJsonArray()
							.toString().replace("\"", "").replace("[", "").replace("]", "").split(",")));
				}

				response.close();
			}
		}
		return new ArrayList<String>();
	}

	private void savePatterns(JsonArray patterns, Shell shell) {
		String selectedDir = null;
		DirectoryDialog dirDialog = new DirectoryDialog(shell);
		dirDialog.setText("Choose a folder where to save the deleted event patterns code.");
		selectedDir = dirDialog.open();

		String patternsString = "";
		for (int i = 0; i < patterns.size(); i++)
			patternsString += patterns.get(i).getAsString() + "\n";

		if (selectedDir == null || patternsString.equalsIgnoreCase("")) {
			MessageDialog.openError(shell, "Delete Event Pattern",
					"There was an error selecting the saving folder or with the pattern EPL code. Please try again.");
		} else {
			EventPatternsStatus.setGeneratedEventPatternPath(selectedDir);
			try {
				FileUtil.write(new File(EventPatternsStatus.getGeneratedEventPatternPath(), "patternsBackup.txt"),
						patternsString);
				MessageDialog.openInformation(shell, "Delete Event Pattern", "The pattern code has been saved.");
			} catch (IOException e) {
				System.out.println("Exception saving pattern: " + e.getMessage());
			}
		}
	}
}
