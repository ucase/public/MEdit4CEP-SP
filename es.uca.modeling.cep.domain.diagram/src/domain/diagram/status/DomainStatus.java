/*******************************************************************************
 * Copyright (c) 2011, 2019 Juan Boubeta-Puig, David Corral-Plaza
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0 
 * which accompanies this distribution, and is available at 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Juan Boubeta-Puig - modifications marked as @generated NOT
 *     David Corral-Plaza - additions
 ******************************************************************************/

package domain.diagram.status;

import java.util.ArrayList;

import domain.Event;

public class DomainStatus {

	private static Boolean domainModified = false;
	private static Event activeEvent = null;

	private static ArrayList<Event> eventsToModify = new ArrayList<>();

	public static Boolean getDomainModified() {
		return domainModified;
	}

	public static void setDomainModified(Boolean value) {
		domainModified = value;
	}

	public static void setActiveEvent(Event value) {
		activeEvent = value;
	}

	public static Event getActiveEvent() {
		return activeEvent;
	}

	public static ArrayList<Event> getEventsToModify() {
		return eventsToModify;
	}

	public static void addEventToModify(Event eventToModify) {
		if (!existsEventToModify(eventToModify.getTypeName()) && eventToModify.getMongodbId() != null)
			eventsToModify.add(eventToModify);
	}

	public static boolean existsEventToModify(String typeName) {
		return eventsToModify.stream().filter(e -> e.getTypeName().equalsIgnoreCase(typeName)).count() >= 1;
	}

	public static void removeEventToModify(String typeName) {
		int index = -1;

		for (int i = 0; i < eventsToModify.size(); i++)
			if (eventsToModify.get(i).getTypeName().equalsIgnoreCase(typeName))
				index = i;

		if (index != -1)
			eventsToModify.remove(index);

		if (eventsToModify.size() == 0)
			domainModified = false;
	}

}
