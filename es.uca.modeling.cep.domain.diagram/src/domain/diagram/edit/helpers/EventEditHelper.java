/*******************************************************************************
 * Copyright (c) 2019 David Corral-Plaza
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0 
 * which accompanies this distribution, and is available at 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Corral-Plaza  - modifications marked as @generated NOT
 ******************************************************************************/
package domain.diagram.edit.helpers;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;

import domain.Event;
import domain.diagram.status.DomainStatus;

/**
 * @generated NOT
 */
public class EventEditHelper extends DomainBaseEditHelper {
	
	protected ICommand getDestroyElementCommand(DestroyElementRequest req) {
		ICommand result = null;	
		
		EObject event = req.getElementToDestroy();
		
		if (event.getClass().getSimpleName().equals("EventImpl")) {
			System.out.println("Setting simple event type as active event to destroy: " + event.toString());
			DomainStatus.setActiveEvent((Event) event);
		}
		
		return result;
	}
	
}
