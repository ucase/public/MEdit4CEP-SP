package domain.diagram.edit.helpers;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;

import domain.Event;
import domain.diagram.status.DomainStatus;

/**
 * @generated NOT
 */
public class EventPropertyEditHelper extends DomainBaseEditHelper {

	protected ICommand getInsteadCommand(IEditCommandRequest req) {
		ICommand result = null;

		EObject event = (EObject) req.getElementsToEdit().get(0);

		if (event.getClass().getSimpleName().equals("EventImpl")) {
			System.out.println("Setting simple event type as active event to modify: " + event.toString());
			DomainStatus.setActiveEvent((Event) event);
		}

		return result;
	}
}
