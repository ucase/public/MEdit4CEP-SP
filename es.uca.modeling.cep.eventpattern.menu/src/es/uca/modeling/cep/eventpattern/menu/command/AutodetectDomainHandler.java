/*******************************************************************************
 * Copyright (c) 2011, 2019 Juan Boubeta-Puig & David Corral-Plaza
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0 
 * which accompanies this distribution, and is available at 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Juan Boubeta-Puig - initial API and implementation
 *     David Corral-Plaza - modifications
 ******************************************************************************/

package es.uca.modeling.cep.eventpattern.menu.command;

import java.io.File;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import domain.CEPDomain;
import domain.DomainFactory;
import domain.DomainPackage;
import domain.Event;
import domain.EventProperty;
import domain.PropertyTypeValue;
import domain.diagram.part.DomainDiagramEditorUtil;
import es.uca.modeling.cep.utils.*;
import es.uca.modeling.cep.eventpattern.menu.dialog.AutodetectDomainDialog;
import okhttp3.Response;

public class AutodetectDomainHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
		IWorkspaceRoot myWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();

		IProject domainProject = myWorkspaceRoot.getProject("domain");

		try {
			if (!domainProject.exists()) {
				AutodetectDomainDialog dialog = new AutodetectDomainDialog(shell);
				dialog.create();

				if (dialog.open() != Window.OK) {
					return null;
				}

				Response response = HTTPRequest.GET("/eventTypes");

				if (!response.isSuccessful() || response == null) {
					MessageDialog.openError(shell, "Auto-detect Domain",
							"There was a problem connecting with the API. Error message: \"" + response.body().string());
					return null;
				}

				String jsonString = response.body().string();
				response.close();

				// Parse response
				Gson gson = new Gson();
				JsonObject[] simpleEvents = gson.fromJson(jsonString, JsonObject[].class);

				String domainName = dialog.getDomainName();
				String domainDescription = dialog.getDomainDescription();
				domainProject.create(null);

				// Open if necessary
				if (!domainProject.isOpen()) {
					domainProject.open(null);
				}

				URI diagramUri = URI.createPlatformResourceURI(
						domainProject.getFile(domainName + ".domain_diagram").getFullPath().toString(), false);
				URI modelUri = URI.createPlatformResourceURI(
						domainProject.getFile(domainName + ".domain").getFullPath().toString(), false);
				Resource diagramResource = DomainDiagramEditorUtil.createDiagram(diagramUri, modelUri,
						new NullProgressMonitor());

				if (diagramResource != null) {
					try {
						// Set model's attributes
						ResourceSet resourceSet = new ResourceSetImpl();
						Resource modelResource = resourceSet.getResource(modelUri, true);
						CEPDomain domain = ((CEPDomain) modelResource.getContents().get(0));
						domain.setDomainName(domainName);
						domain.setDomainCreationDate(new Date());
						domain.setDomainDescription(domainDescription);

						int n = simpleEvents.length;

						if (n == 0) {
							MessageDialog.openInformation(shell, "Auto-detect Domain",
									"There are no event types yet for the domain '" + domain.getDomainName() + "'.");
						}

						for (int i = 0; i < n; i++) {
							JsonObject simpleEvent = simpleEvents[i];
							JsonObject schema = simpleEvent.getAsJsonObject("schema");
							String eventTypeName = schema.get("name").getAsString();
							System.out.println("Simple Event Name: " + eventTypeName);

							Map<String, Object> eventType = new LinkedHashMap<String, Object>();
							Map<String, Object> eventTypeAttributes = new LinkedHashMap<String, Object>();
							JsonArray attributes = schema.getAsJsonArray("fields");

							for (JsonElement att : attributes) {
								System.out.println("Atributte: " + att.getAsJsonObject().get("name").getAsString());
								eventTypeAttributes.put(att.getAsJsonObject().get("name").getAsString(),
										getClassType(att.getAsJsonObject().get("type").getAsString()));
							}

							eventType.put(eventTypeName, eventTypeAttributes);

							DomainPackage.eINSTANCE.eClass();
							DomainFactory factory = DomainFactory.eINSTANCE;

							domain.Event domainEvent = factory.createEvent();
							domainEvent.setTypeName(eventTypeName);

							domainEvent = (Event) getDomainEvent(eventTypeName, eventType, domainEvent, factory);
							domainEvent.setMongodbId(simpleEvent.get("_id").getAsString());
							System.out.println("SETTING ID: " + simpleEvent.get("_id").getAsString());
							domain.getEvents().add(domainEvent);
						}

						modelResource.save(null);
						DomainDiagramEditorUtil.openDiagram(diagramResource);
					} catch (PartInitException e) {
						ErrorDialog.openError(shell, "Auto-detect Domain", null, e.getStatus());
					}
				}
			} else {
				if (MessageDialog.openConfirm(shell, "Auto-detect Domain",
						"The editor has already been customised. Would you like to update the existing domain and retrieve new Simple Events?")) {
					// Open if necessary
					if (!domainProject.isOpen()) {
						domainProject.open(null);
					}

					File currentDir = new File(domainProject.getLocationURI());
					File[] files = currentDir.listFiles();

					for (File domainFile : files) {
						if (!domainFile.isDirectory() && domainFile.getName().matches(".+domain_diagram")) {
							URI diagramUri = URI.createPlatformResourceURI(
									domainProject.getFile(domainFile.getName()).getFullPath().toString(), false);
							URI modelUri = URI.createPlatformResourceURI(domainProject
									.getFile(domainFile.getName().replace("_diagram", "")).getFullPath().toString(),
									false);

							ResourceSet resourceSet = new ResourceSetImpl();
							Resource diagramResource = resourceSet.getResource(diagramUri, true);
							Resource modelResource = resourceSet.getResource(modelUri, true);
							CEPDomain domain = ((CEPDomain) modelResource.getContents().get(0));

							DomainPackage.eINSTANCE.eClass();
							// Get the default factory singleton
							DomainFactory factory = DomainFactory.eINSTANCE;

							// Repeat
							boolean updatedDomain = false;
							
							Response response = HTTPRequest.GET("/eventTypes/domain/" + domain.getDomainName());

							if (!response.isSuccessful() || response == null) {
								MessageDialog.openError(shell, "Auto-detect Domain",
										"There was a problem connecting with the API. Error message: \"" + response.body().string());
								return null;
							}

							String jsonString = response.body().string();
							response.close();

							// Parse response
							Gson gson = new Gson();
							JsonObject[] simpleEvents = gson.fromJson(jsonString, JsonObject[].class);

							int n = simpleEvents.length;

							if (n == 0) {
								MessageDialog.openError(shell, "Auto-detect Domain",
										"There are no event types to create the domain '" + domain.getDomainName()
												+ "'.");
								return null;
							}

							for (int i = 0; i < n; i++) {
								boolean eventExist = false;

								JsonObject simpleEvent = simpleEvents[i];
								JsonObject schema = simpleEvent.getAsJsonObject("schema");
								String eventTypeName = schema.get("name").getAsString();
								System.out.println("Simple Event Name to add: " + eventTypeName);

								for (Iterator iter = EcoreUtil.getAllContents(modelResource, true); iter.hasNext();) {
									EObject eObject = (EObject) iter.next();

									if (eObject.getClass().getSimpleName().equals("EventImpl")) {
										Event e = ((Event) eObject);

										System.out.println("Comparing " + eventTypeName + " with " + e.getTypeName());
										if (e.getTypeName().equals(eventTypeName)) {
											eventExist = true;
											updatedDomain = false;
										} else {
											eventExist = false;
											updatedDomain = true;
										}
									}
								}

								if (!eventExist) {
									System.out.println("Adding new event type: " + eventTypeName);
									Map<String, Object> eventType = new LinkedHashMap<String, Object>();
									Map<String, Object> eventTypeAttributes = new LinkedHashMap<String, Object>();
									JsonArray attributes = schema.getAsJsonArray("fields");

									for (JsonElement att : attributes) {
										System.out.println(
												"Atributte: " + att.getAsJsonObject().get("name").getAsString());
										eventTypeAttributes.put(att.getAsJsonObject().get("name").getAsString(),
												getClassType(att.getAsJsonObject().get("type").getAsString()));
									}

									eventType.put(eventTypeName, eventTypeAttributes);

									domain.Event domainEvent = factory.createEvent();
									domainEvent.setTypeName(eventTypeName);

									domainEvent = (Event) getDomainEvent(eventTypeName, eventType, domainEvent,
											factory);
									domainEvent.setMongodbId(simpleEvent.get("_id").getAsString());
									domain.getEvents().add(domainEvent);
								}
							}

							if (updatedDomain) {
								modelResource.save(null);
								DomainDiagramEditorUtil.openDiagram(diagramResource);
							} else {
								MessageDialog.openInformation(shell, "Auto-detect Domain",
										"The domain has not been updated.");
							}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}

	private Object getClassType(String type) {
		switch (type) {
		case "string":
			return String.class;
		case "int":
			return Integer.class;
		case "long":
			return Long.class;
		case "float":
			return Float.class;
		case "double":
			return Double.class;
		default:
			return String.class;
		}
	}

	private Object getDomainEvent(String eventTypeName, Map<String, Object> eventType, Object node,
			DomainFactory factory) {
		for (String key : eventType.keySet()) {
			Object value = eventType.get(key);

			if (value instanceof Map) {

				if (key.equals(eventTypeName)) {
					getDomainEvent(eventTypeName, (Map<String, Object>) value, node, factory);
				} else {
					EventProperty eventProperty = factory.createEventProperty();
					eventProperty.setName(key);

					if (node.getClass().getSimpleName().equals("EventImpl")) {
						((Event) node).getEventProperties().add(eventProperty);
					} else if (node.getClass().getSimpleName().equals("EventPropertyImpl")) {
						((EventProperty) node).getEventProperties().add(eventProperty);
					}

					getDomainEvent(eventTypeName, (Map<String, Object>) value, eventProperty, factory);
				}

			} else {

				EventProperty eventProperty = factory.createEventProperty();
				eventProperty.setName(key);

				if (value.toString().compareTo("class java.lang.String") == 0) {
					eventProperty.setType(PropertyTypeValue.STRING);
				} else if (value.toString().compareTo("class java.lang.Double") == 0) {
					eventProperty.setType(PropertyTypeValue.DOUBLE);
				} else if (value.toString().compareTo("class java.lang.Float") == 0) {
					eventProperty.setType(PropertyTypeValue.FLOAT);
				} else if (value.toString().compareTo("class java.lang.Long") == 0) {
					eventProperty.setType(PropertyTypeValue.LONG);
				} else if (value.toString().compareTo("class java.lang.Integer") == 0) {
					eventProperty.setType(PropertyTypeValue.INTEGER);
				} else if (value.toString().compareTo("class java.lang.Boolean") == 0) {
					eventProperty.setType(PropertyTypeValue.BOOLEAN);
				}

				System.out.println("node.getClass().getSimpleName(): " + node.getClass().getSimpleName());

				if (node.getClass().getSimpleName().equals("EventImpl")) {
					((Event) node).getEventProperties().add(eventProperty);
				} else if (node.getClass().getSimpleName().equals("EventPropertyImpl")) {
					((EventProperty) node).getEventProperties().add(eventProperty);
				}
			}
		}

		return node;
	}
}
