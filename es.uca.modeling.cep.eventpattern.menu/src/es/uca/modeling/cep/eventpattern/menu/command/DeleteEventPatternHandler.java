/*******************************************************************************
 * Copyright (c) 2011, 2019 Juan Boubeta-Puig & David Corral-Plaza
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0 
 * which accompanies this distribution, and is available at 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Juan Boubeta-Puig - initial API and implementation
 *     David Corral-Plaza - modifications
 ******************************************************************************/

package es.uca.modeling.cep.eventpattern.menu.command;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.epsilon.egl.util.FileUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISources;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import eventpattern.impl.EventImpl;
import eventpattern.impl.EventPropertyImpl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import domain.CEPDomain;
import eventpattern.CEPEventPattern;
import eventpattern.Link;
import eventpattern.diagram.part.EventpatternDiagramEditor;
import eventpattern.diagram.part.EventpatternDiagramEditorUtil;
import eventpattern.diagram.status.EventPatternsStatus;
import okhttp3.Response;
import es.uca.modeling.cep.utils.*;

public class DeleteEventPatternHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		try {
			Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();

			String domainName = EventPatternsStatus.getDomainName();
			IWorkspaceRoot myWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
			IProject patternProject = myWorkspaceRoot.getProject(domainName + "_patterns");

			if (!patternProject.exists()) {
				MessageDialog.openError(shell, "Delete Event Pattern", "There are no event patterns to be deleted.");
				return null;
			}

			if (!HandlerUtil.getActiveEditor(event).getClass().getName()
					.equals("eventpattern.diagram.part.EventpatternDiagramEditor")) {
				MessageDialog.openError(shell, "Delete Event Pattern", "An event pattern must be open.");
				return null;
			}

			if (MessageDialog.openConfirm(shell, "Confirm",
					"Are you sure that you want to permanently delete this pattern? This action will remove the pattern "
							+ "from the CEP engine and the API. Note also that dependent patterns (if any) will be removed from the API and "
							+ "the CEP engine, but not from MEdit4CEP-SP, so you can edit, save and deploy them again.")) {
				// Obtain the active editor's diagram
				EventpatternDiagramEditor patternDiagramEditor = (EventpatternDiagramEditor) HandlerUtil
						.getActiveEditor(event);

				if (patternDiagramEditor == null || !patternDiagramEditor.getTitle().endsWith("pattern_diagram")) {
					MessageDialog.openError(shell, "Delete Event Pattern", "An event pattern must be open.");
					return null;
				}

				String activePatternName = patternDiagramEditor.getTitle().replace(".pattern_diagram", "");
				IProject complexEventProject = myWorkspaceRoot.getProject(domainName + "_complex_events");
				IFile complexEventFile = complexEventProject
						.getFile(domainName + "_complex_events" + ".domain_diagram");
				IFile activePatternModelFile = patternProject.getFile(activePatternName + ".pattern");
				IFile activePatternDiagramFile = patternProject.getFile(activePatternName + ".pattern_diagram");

				// Open if necessary
				if (!patternProject.isOpen()) {
					patternProject.open(null);
				}

				ResourceSet resourceSet = new ResourceSetImpl();
				URI activePatternModelUri = URI.createPlatformResourceURI(
						patternProject.getFile(activePatternName + ".pattern").getFullPath().toString(), false);
				Resource patternModelResource = resourceSet.getResource(activePatternModelUri, true);
				CEPEventPattern eventPatternModel = (CEPEventPattern) patternModelResource.getContents().get(0);
				String patternId = eventPatternModel.getMongodbId();

				// Remove active pattern from file
				activePatternModelFile = patternProject.getFile(activePatternName + ".pattern");
				activePatternDiagramFile = patternProject.getFile(activePatternName + ".pattern_diagram");
				activePatternModelFile.delete(true, null);
				activePatternDiagramFile.delete(true, null);

				ArrayList<String> patternsToRemove = removePattern(patternId, shell);

				// Clean affected patterns
				for (int i = 0; i < patternsToRemove.size() - 1; i++) {
					String patternToRemove = patternsToRemove.get(i);
					System.out.println("Cleaning " + patternToRemove);
					URI activePatternModelUriAux = URI.createPlatformResourceURI(
							patternProject.getFile(patternToRemove + ".pattern").getFullPath().toString(),
							false);
					Resource patternModelResourceAux = resourceSet.getResource(activePatternModelUriAux,
							true);
					CEPEventPattern eventPatternModelAux = (CEPEventPattern) patternModelResourceAux
							.getContents().get(0);

					// Set pattern MongoDB ID to null
					eventPatternModelAux.setMongodbId(null);
					ArrayList<EObject> elementsToRemove = new ArrayList<>();
					for (TreeIterator iterator = EcoreUtil.getAllContents(eventPatternModelAux,
							true); iterator.hasNext();) {
						EObject eObjt = (EObject) iterator.next();

						if (eObjt.getClass().getSimpleName().equals("EventImpl")) {
							eventpattern.impl.EventImpl Event = (EventImpl) eObjt;
							if (patternsToRemove.contains(Event.getTypeName())) {
								elementsToRemove.addAll(Event.getOutboundLink());
								elementsToRemove.add(Event);

								// Remove event properties and their links
								for (TreeIterator iterAux = eObjt.eAllContents(); iterAux.hasNext();) {
									EObject eObjectAux = (EObject) iterAux.next();

									if (eObjectAux.getClass().getSimpleName().equals("EventPropertyImpl")) {
										eventpattern.impl.EventPropertyImpl EventProperty = (EventPropertyImpl) eObjectAux;

										elementsToRemove.add(EventProperty);
										elementsToRemove.addAll(EventProperty.getOutboundLink());
									}
								}
							}
						}
					}

					EcoreUtil.removeAll(elementsToRemove);

					try {
						patternModelResourceAux.save(null);
					} catch (Exception e1) {
						System.out.println("Exception: " + e1.getMessage());
					}
				}

				URI complexEventModelUri = URI.createPlatformResourceURI(complexEventProject
						.getFile(domainName + "_complex_events" + ".domain").getFullPath().toString(), false);
				Resource complexEventModelResource = resourceSet.getResource(complexEventModelUri, true);
				CEPDomain domainModel = (CEPDomain) complexEventModelResource.getContents().get(0);

				// Clean affected complex events
				ArrayList<domain.Event> complexEventsToRemove = new ArrayList<>();
				for (TreeIterator iter = EcoreUtil.getAllContents(domainModel, true); iter.hasNext();) {
					EObject eObject = (EObject) iter.next();

					if (eObject.getClass().getSimpleName().equals("EventImpl")) {
						domain.Event domainEvent = (domain.Event) eObject;
						if (patternsToRemove.contains(domainEvent.getTypeName())) {
							complexEventsToRemove.add(domainEvent);
						}
					}
				}

				EcoreUtil.removeAll(complexEventsToRemove);
				complexEventModelResource.save(null);
				
				// Update palettes
				IProject domainProject = myWorkspaceRoot.getProject("domain");
				File currentDir = new File(domainProject.getLocationURI());

				// Open if necessary
				if (!patternProject.isOpen()) {
					patternProject.open(null);
				}

				// Change the palette of all existing editors
				currentDir = new File(patternProject.getLocationURI());
				File[] filesAux = currentDir.listFiles();
				System.out.println("Files size " + filesAux.length);
				for (File file : filesAux) {
					System.out.println("Name: " + file.getName());
					// Refresh simple event tools for all event patterns
					if (!file.isDirectory() && file.getName().matches(".+pattern_diagram")) {
						URI diagramUri = URI.createPlatformResourceURI(
								patternProject.getFile(file.getName()).getFullPath().toString(), false);

						Resource diagramResource = new ResourceSetImpl().getResource(diagramUri, true);
						EventpatternDiagramEditorUtil.openDiagram(diagramResource);
						
						IWorkbench workbench = PlatformUI.getWorkbench();
						IWorkbenchWindow activeWindow = workbench.getActiveWorkbenchWindow();
						IWorkbenchPage activePage = activeWindow.getActivePage();
						IEditorPart activeEditor = activePage.getActiveEditor();
						
						EventpatternDiagramEditor diagramEditor = (EventpatternDiagramEditor) activeEditor;
						
						System.out.println("diagramEditor = " + diagramEditor);
						System.out.println("Refreshing complex event palette of " + file.getName());
						diagramEditor.refreshPaletteComplexEvent();
					}
				}
			}

		} catch (CoreException | IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public ArrayList<String> removePattern(String id, Shell shell) {
		Response response = HTTPRequest.DELETE("/patterns/" + id);

		if (response.code() == 500)
			MessageDialog.openError(shell, "Delete Event Pattern",
					"There was an error while removing the event pattern in the API. Please try again.");
		else if (response.code() == 404)
			MessageDialog.openError(shell, "Delete Event Pattern",
					"The event pattern id was not found in the API. Please check it and try again.");
		else if (response.code() == 200) {
			String jsonString;

			try {
				jsonString = response.body().string();
			} catch (IOException e) {
				e.printStackTrace();
				return new ArrayList<String>();
			}

			Gson gson = new Gson();
			JsonObject jsonRes = gson.fromJson(jsonString, JsonObject.class);
			response.close();

			if (MessageDialog.openConfirm(shell, "Backup Patterns",
					"Would you like to backup the pattern that is going to be removed?"))
				savePattern(jsonRes.get("patterns").getAsJsonArray(), shell);

			if (KafkaCEP.send("streams-undeploy", jsonRes.get("deploymentId").getAsString()))
				MessageDialog.openInformation(shell, "Delete Event Pattern",
						"The pattern has been deleted and it has been undeployed from the CEP engine.");
			else
				MessageDialog.openError(shell, "Delete Event Pattern",
						"There was an error undeploying the pattern from the CEP engine. Please try again.");

			response.close();
			System.out.println("Patterns to remove: " + jsonRes.get("patternsToRemove").getAsJsonArray());
			return new ArrayList<String>(Arrays.asList(jsonRes.get("patternsToRemove").getAsJsonArray().toString()
					.replace("\"", "").replace("[", "").replace("]", "").split(",")));
		}

		response.close();

		return new ArrayList<String>();
	}

	private void savePattern(JsonArray patterns, Shell shell) {
		String pattern = "";
		for (int i = 0; i < patterns.size(); i++)
			pattern += patterns.get(i).getAsString() + "\n";

		String selectedDir = null;
		DirectoryDialog dirDialog = new DirectoryDialog(shell);
		dirDialog.setText("Choose a folder where to save the deleted event pattern code.");
		selectedDir = dirDialog.open();

		if (selectedDir == null || pattern == null) {
			MessageDialog.openError(shell, "Delete Event Pattern",
					"There was an error selecting the saving folder or with the pattern EPL code. Please try again.");
		} else {
			EventPatternsStatus.setGeneratedEventPatternPath(selectedDir);
			try {
				FileUtil.write(new File(EventPatternsStatus.getGeneratedEventPatternPath(), "patternsBackup.txt"),
						pattern);
				MessageDialog.openInformation(shell, "Delete Event Pattern", "The pattern code has been saved.");
			} catch (IOException e) {
				System.out.println("Exception saving pattern: " + e.getMessage());
			}
		}
	}
}