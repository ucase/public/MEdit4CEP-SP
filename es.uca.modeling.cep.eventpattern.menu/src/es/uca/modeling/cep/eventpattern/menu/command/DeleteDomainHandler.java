/*******************************************************************************
 * Copyright (c) 2011, 2019 Juan Boubeta-Puig & David Corral-Plaza
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0 
 * which accompanies this distribution, and is available at 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Juan Boubeta-Puig - initial API and implementation
 *     David Corral-Plaza - modifications
 ******************************************************************************/

package es.uca.modeling.cep.eventpattern.menu.command;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.epsilon.egl.util.FileUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import eventpattern.diagram.status.EventPatternsStatus;
import okhttp3.Response;
import es.uca.modeling.cep.utils.*;

public class DeleteDomainHandler extends AbstractHandler {


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		try {
			
			String domainName = EventPatternsStatus.getDomainName();
			Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
			IWorkspaceRoot myWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
			IProject domainProject = myWorkspaceRoot.getProject("domain");
			IProject patternProject = myWorkspaceRoot.getProject(domainName + "_patterns");
			IProject complexEventProject = myWorkspaceRoot.getProject(domainName + "_complex_events");

			if (!domainProject.exists()) {
				MessageDialog.openError(shell, "Delete CEP Domain", "Domain does not exist.");
				return null;
			} else {
				if (!HandlerUtil.getActiveEditor(event).getClass().getName()
						.equals("domain.diagram.part.DomainDiagramEditor")) {

					MessageDialog.openError(shell, "Delete CEP Domain", "The domain must be open.");
					return null;
				}

				// Open if necessary
				if (!domainProject.isOpen()) {
					domainProject.open(null);
				}

				if (MessageDialog.openConfirm(shell, "Delete CEP Domain",
						"Are you sure that you want to permanently delete the domain? "
								+ "This action will remove all the patterns and event types related to this domain from the CEP engine and the API")) {

					domainProject.delete(true, null);
					patternProject.delete(true, null);
					complexEventProject.delete(true, null);
					myWorkspaceRoot.refreshLocal(2, null);
					removeDomain(EventPatternsStatus.getDomainName(), shell);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public void removeDomain(String name, Shell shell) {
		Response response = HTTPRequest.DELETE("/domains/" + name);

		if (response.code() == 500)
			MessageDialog.openError(shell, "Delete CEP Domain",
					"There was an error while removing the domain in the API. Please try again.");
		else if (response.code() == 404)
			MessageDialog.openError(shell, "Delete CEP Domain",
					"The event domain was not found in the API. Please check it and try again.");
		else if (response.code() == 200) {
			String jsonString;
			
			try {
				jsonString = response.body().string();
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
			
			Gson gson = new Gson();
			JsonObject jsonRes = gson.fromJson(jsonString, JsonObject.class);
			response.close();
			
			if (MessageDialog.openConfirm(shell, "Backup Patterns",
					"Would you like to backup the patterns that are going to be removed?"))
				savePatterns(jsonRes.get("patterns").getAsJsonArray(), shell);

			if (KafkaCEP.send("streams-undeploy", jsonRes.get("deploymentIds").getAsJsonArray()))
				MessageDialog.openInformation(shell, "Delete CEP Domain",
						"The domain has been deleted and all has been undeployed from the CEP engine.");
			else
				MessageDialog.openError(shell, "Delete CEP Domain",
						"There was an error undeploying from the CEP engine. Please try again.");
		}

		response.close();
	}
	
	private void savePatterns(JsonArray patterns, Shell shell) {
		String selectedDir = null;
		DirectoryDialog dirDialog = new DirectoryDialog(shell);
		dirDialog.setText("Choose a folder where to save the deleted event patterns code.");
		selectedDir = dirDialog.open();
		
		String patternsString = "";
		for (int i = 0; i < patterns.size(); i++)
			patternsString += patterns.get(i).getAsString() + "\n"; 
		
		if (selectedDir == null || patternsString.equalsIgnoreCase("")) {
			MessageDialog.openError(shell, "Delete Event Pattern",
					"There was an error selecting the saving folder or with the pattern EPL code. Please try again.");
		} else {
			EventPatternsStatus.setGeneratedEventPatternPath(selectedDir);
			try {
				FileUtil.write(new File(EventPatternsStatus.getGeneratedEventPatternPath(),
						"patternsBackup.txt"), patternsString);
				MessageDialog.openInformation(shell, "Delete Event Pattern",
						"The pattern code has been saved.");
			} catch (IOException e) {
				System.out.println("Exception saving pattern: " + e.getMessage());
			}
		}
	}
}
