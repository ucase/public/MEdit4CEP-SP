/*******************************************************************************
 * Copyright (c) 2011, 2019 Juan Boubeta-Puig & David Corral-Plaza
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0 
 * which accompanies this distribution, and is available at 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Juan Boubeta-Puig - initial API and implementation
 *     David Corral-Plaza - modifications
 ******************************************************************************/

package es.uca.modeling.cep.eventpattern.menu.command;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import domain.CEPDomain;
import domain.Event;
import domain.EventProperty;
import domain.diagram.part.DomainDiagramEditor;
import domain.diagram.part.DomainDiagramEditorUtil;
import domain.diagram.status.DomainStatus;
import eventpattern.CEPEventPattern;
import eventpattern.diagram.part.EventpatternDiagramEditor;
import eventpattern.diagram.part.EventpatternDiagramEditorUtil;
import eventpattern.diagram.status.EventPatternsStatus;
import eventpattern.impl.EventImpl;
import eventpattern.impl.EventPropertyImpl;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.Response;
import es.uca.modeling.cep.utils.*;

public class SaveValidateDomainHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();

		// 1� Obtain the active editor's diagram

		DomainDiagramEditor domainDiagramEditor = (DomainDiagramEditor) HandlerUtil.getActiveEditor(event);

		if (domainDiagramEditor == null || !domainDiagramEditor.getTitle().endsWith("domain_diagram")) {
			MessageDialog.openError(shell, "Save and Validate CEP Domain", "The domain must be open.");
			return null;
		}

		// 2� Save all changes made in the editor

		domainDiagramEditor.doSave(new NullProgressMonitor());

		// 3� Check if there are some problems which must be solved

		IResource ir = (IResource) HandlerUtil.getActiveEditorInput(event).getAdapter(IResource.class);
		IMarker[] problems = null;
		int depth = IResource.DEPTH_INFINITE;

		try {
			problems = ir.findMarkers(IMarker.PROBLEM, true, depth);
		} catch (CoreException e1) {
			e1.printStackTrace();
		}

		if (problems.length > 0) {
			MessageDialog.openError(shell, "Save and Validate CEP Domain",
					"There are some problems that must be solved before saving the domain.");

			try {
				HandlerUtil.getActiveWorkbenchWindowChecked(event).getActivePage()
						.showView("org.eclipse.ui.views.ProblemView");
			} catch (PartInitException e) {
				e.printStackTrace();
			}

			return null;
		}
		
		IWorkspaceRoot myWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		String domainName = EventPatternsStatus.getDomainName();
		IProject patternProject = myWorkspaceRoot.getProject(domainName + "_patterns");
		IProject domainProject = myWorkspaceRoot.getProject("domain");
		ResourceSet resourceSet = new ResourceSetImpl();
		
		File currentDir = new File(domainProject.getLocationURI());
		ArrayList<File> files = new ArrayList<File>(Arrays.asList(currentDir.listFiles()));

		files.removeIf(f -> !f.getName().matches(".+domain_diagram"));

		if (files.size() >= 0) {
			File domainFile = files.get(0);
			URI modelUri = URI.createPlatformResourceURI(
					domainProject.getFile(domainFile.getName().replace("_diagram", "")).getFullPath().toString(),
					false);

			Resource modelResource = resourceSet.getResource(modelUri, true);

			for (Iterator iter = EcoreUtil.getAllContents(modelResource, true); iter.hasNext();) {
				EObject eObject = (EObject) iter.next();

				if (eObject.getClass().getSimpleName().equals("EventImpl")) {
					Event e = ((Event) eObject);
					System.out.println("MongoDB ID: " + e.getMongodbId());
					System.out.println("Simple Event name: " + e.getTypeName());

					if (DomainStatus.getDomainModified() && DomainStatus.existsEventToModify(e.getTypeName())) {
						// UPDATE a existing simple event
						MessageDialog.open(2, shell, "Modifying Event Type",
								"A change in the Simple Event Type '" + e.getTypeName() + "' has been detected. "
										+ "Due to this Simple Event Type has been already deployed in the CEP engine, it is mandatory to remove it from the engine "
										+ "and redeploy it, to avoid errors. Likewise, all the patterns that depend on this Simple Event Type will be removed"
										+ " from the CEP engine and the API, but not from MEdit4CEP-SP, so you can edit, save and deploy them again.",
								SWT.NONE);
					
						ArrayList<String> patternsToRemove = removeEventType(e.getMongodbId(), shell, domainName);

						// Clean affected patterns
						for (int i = 0; i < patternsToRemove.size() - 1; i++) {
							String patternToRemove = patternsToRemove.get(i);
							System.out.println("Cleaning " + patternToRemove);
							URI activePatternModelUriAux = URI.createPlatformResourceURI(
									patternProject.getFile(patternToRemove + ".pattern").getFullPath().toString(),
									false);
							Resource patternModelResourceAux = resourceSet.getResource(activePatternModelUriAux, true);
							CEPEventPattern eventPatternModelAux = (CEPEventPattern) patternModelResourceAux
									.getContents().get(0);

							// Set pattern MongoDB ID to null
							eventPatternModelAux.setMongodbId(null);
							ArrayList<EObject> elementsToRemove = new ArrayList<>();
							for (TreeIterator iterator = EcoreUtil.getAllContents(eventPatternModelAux, true); iterator.hasNext();) {
								EObject eObjt = (EObject) iterator.next();

								if (eObjt.getClass().getSimpleName().equals("EventImpl")) {
									eventpattern.impl.EventImpl Event = (EventImpl) eObjt;
									if (patternsToRemove.contains(Event.getTypeName())) {
										elementsToRemove.addAll(Event.getOutboundLink());
										elementsToRemove.add(Event);

										// Remove event properties and their links
										for (TreeIterator iterAux = eObjt.eAllContents(); iterAux.hasNext();) {
											EObject eObjectAux = (EObject) iterAux.next();

											if (eObjectAux.getClass().getSimpleName().equals("EventPropertyImpl")) {
												eventpattern.impl.EventPropertyImpl EventProperty = (EventPropertyImpl) eObjectAux;

												elementsToRemove.add(EventProperty);
												elementsToRemove.addAll(EventProperty.getOutboundLink());
											}
										}
									}
								}
							}

							EcoreUtil.removeAll(elementsToRemove);

							try {
								patternModelResourceAux.save(null);
							} catch (Exception e1) {
								System.out.println("Exception: " + e1.getMessage());
							}
						}

						if (createEventType(e, shell, modelResource, domainName))
							DomainStatus.removeEventToModify(e.getTypeName());
						
						DomainStatus.setDomainModified(true);
						
						// Clean affected complex events domain diagram
						IProject complexEventProject = myWorkspaceRoot.getProject(domainName + "_complex_events");
						IFile complexEventFile = complexEventProject.getFile(domainName + "_complex_events" + ".domain_diagram");
						URI complexEventModelUri = URI.createPlatformResourceURI(
								complexEventProject.getFile(domainName + "_complex_events" + ".domain").getFullPath().toString(),
								false);
						Resource complexEventModelResource = resourceSet.getResource(complexEventModelUri, true);
						CEPDomain domainModel = (CEPDomain) complexEventModelResource.getContents().get(0);
						
						ArrayList<domain.Event> complexEventsToRemove = new ArrayList<>();
						for (TreeIterator iterator = EcoreUtil.getAllContents(domainModel, true); iterator.hasNext();) {
							EObject eObjct = (EObject) iterator.next();

							if (eObjct.getClass().getSimpleName().equals("EventImpl")) {
								domain.Event domainEvent = (domain.Event) eObjct;
								if (patternsToRemove.contains(domainEvent.getTypeName())) {
									complexEventsToRemove.add(domainEvent);
								}
							}
						}

						EcoreUtil.removeAll(complexEventsToRemove);
						
						try {
							complexEventModelResource.save(null);
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					} else if (e.getMongodbId() == null) {
						// POST the new simple event type
						createEventType(e, shell, modelResource, domainName);
					} else {
						// UPDATE the existing simple event type's domain
						updateEventType(e, shell, domainName);
					}
				}
			}
		}

		// If the domain has been modified, then the simple event tools of every event
		// pattern's palette will be updated.
		if (DomainStatus.getDomainModified() != null && DomainStatus.getDomainModified() && patternProject.exists()) {
			// Update domain for simple events
			domainProject = myWorkspaceRoot.getProject("domain");
			currentDir = new File(domainProject.getLocationURI());

			try {
				// Open if necessary
				if (!patternProject.isOpen()) {
					patternProject.open(null);
				}

				// Change the palette of all existing editors
				currentDir = new File(patternProject.getLocationURI());
				File[] filesAux = currentDir.listFiles();

				for (File file : filesAux) {
					// Refresh simple event tools for all event patterns
					if (!file.isDirectory() && file.getName().matches(".+pattern_diagram")) {
						URI diagramUri = URI.createPlatformResourceURI(
								patternProject.getFile(file.getName()).getFullPath().toString(), false);

						Resource diagramResource = resourceSet.getResource(diagramUri, true);
						EventpatternDiagramEditorUtil.openDiagram(diagramResource);
						
						EventpatternDiagramEditor diagramEditor = (EventpatternDiagramEditor) HandlerUtil
								.getActiveEditor(event);

						System.out.println("Refreshing palettes of " + file.getName());
						diagramEditor.refreshPaletteSimpleEvent();
						diagramEditor.refreshPaletteComplexEvent();
					}
				}

				domainProject = myWorkspaceRoot.getProject("domain");

				// Open if necessary
				if (!domainProject.isOpen()) {
					domainProject.open(null);
				}

				URI domainDiagramUri = URI.createPlatformResourceURI(
						domainProject.getFile(domainName + ".domain_diagram").getFullPath().toString(), false);

				Resource domainDiagramResource = resourceSet.getResource(domainDiagramUri, true);

				DomainDiagramEditorUtil.openDiagram(domainDiagramResource);

				DomainStatus.setDomainModified(false);

			} catch (Exception exception) {
				exception.printStackTrace();
			}
		}

		MessageDialog.openInformation(shell, "Save and Validate CEP Domain",
				"The domain has been correctly saved and validated.");

		return null;
	}

	public ArrayList<String> removeEventType(String id, Shell shell, String domainName) {
		Response response = HTTPRequest.DELETE("/eventTypes/" + id + "/" + domainName);

		if (response.code() == 500)
			MessageDialog.openError(shell, "Edit Event Type",
					"There was an error while editing the event type in the API. Please try again.");
		else if (response.code() == 404)
			MessageDialog.openError(shell, "Edit Event Type",
					"The event type was not found in the API. Please check it and try again.");
		else if (response.code() == 200) {
			String jsonString;
			try {
				jsonString = response.body().string();
			} catch (IOException e) {
				e.printStackTrace();
				return new ArrayList<>();
			}
			Gson gson = new Gson();
			JsonObject jsonRes = gson.fromJson(jsonString, JsonObject.class);
			response.close();
			
			
			if (KafkaCEP.send("streams-undeploy", jsonRes.get("deploymentId").getAsString()))
				MessageDialog.openInformation(shell, "Edit Event Type",
						"The Event Type has been undeployed from the CEP engine.");
			else
				MessageDialog.openError(shell, "Edit Event Type",
						"There was an error sending the Event Type to undeploy to the CEP engine. Please check it.");
			
			return new ArrayList<String>(Arrays.asList(jsonRes.get("patternsToRemove").getAsJsonArray().toString().replace("\"","").replace("[","").replace("]","").split(",")));
		}

		response.close();
		return new ArrayList<>();
	}

	public boolean createEventType(domain.Event e, Shell shell, Resource modelResource, String domainName) {
		MediaType JSON = MediaType.parse("application/json; charset=utf-8");

		EList<EventProperty> properties = e.getEventProperties();

		String fields = "";

		for (int i = 0; i < properties.size(); i++) {
			EventProperty p = properties.get(i);
			fields += "{" + "	\"name\": \"" + p.getName() + "\"," + "	\"type\": \""
					+ getName(p.getType().getLiteral()) + "\"" + "},";
		}

		fields = fields.substring(0, fields.length() - 1);

		String schema = "{" + "        \"name\": \"" + e.getTypeName() + "\"," + "        \"type\": \"record\","
				+ "        \"fields\": [" + fields + "        ]" + "}";

		String body = "{" + "    \"domain\": \"" + EventPatternsStatus.getDomainName() + "\"," + "    \"timestamp\": \""
				+ System.currentTimeMillis() + "\"," + "    \"schema\": " + schema + "}";

		Response response = HTTPRequest.POST("/eventTypes/", RequestBody.create(JSON, body));

		if (response.code() == 500) {
			MessageDialog.openError(shell, "Save and Validate CEP Domain",
					"An error occurs in the API. Please check the API log and try again.");
		} else if (response.code() == 409) {
			MessageDialog.openError(shell, "Save and Validate CEP Domain", "A event type named " + e.getTypeName()
					+ " already exists on the domain" + domainName + ". Please try again.");
		} else if (response.code() == 404) {
			MessageDialog.openError(shell, "Save and Validate CEP Domain", "There is not a domain with the name "
					+ domainName + " on the API. Please check the API log and try again.");
		} else {
			String jsonString;

			try {
				jsonString = response.body().string();
			} catch (IOException e1) {
				e1.printStackTrace();
				return false;
			}

			Gson gson = new Gson();
			JsonObject jsonRes = gson.fromJson(jsonString, JsonObject.class);
			e.setMongodbId(jsonRes.get("id").getAsString());

			try {
				modelResource.save(null);
			} catch (IOException e1) {
				e1.printStackTrace();
				return false;
			}

			if (KafkaCEP.send("streams-schemas",
					schema.replaceFirst("\\{", "{ \"mongodbId\": \"" + e.getMongodbId() + "\", ")))
				MessageDialog.openInformation(shell, "Save and Validate CEP Domain",
						"The Event Type has been deployed in the CEP engine.");
			else
				MessageDialog.openError(shell, "Save and Validate CEP Domain",
						"There was an error sending the Event Type to the CEP engine. Please check it.");

			return true;
		}

		response.close();

		return false;
	}

	public void updateEventType(domain.Event e, Shell shell, String domainName) {
		Response response = HTTPRequest.UPDATE("/domains/domain/" + domainName + "/eventType/" + e.getMongodbId(),
				RequestBody.create(null, ""));

		if (response.code() == 500) {
			MessageDialog.openError(shell, "Save and Validate CEP Domain",
					"An error occurs in the API. Please check the API log and try again.");
		} else if (response.code() == 404) {
			MessageDialog.openError(shell, "Save and Validate CEP Domain", "There is not a domain with that name");
		}

		response.close();
	}

	private String getName(String literal) {
		switch (literal) {
		case "String":
			return "string";
		case "Integer":
			return "int";
		case "Long":
			return "long";
		case "Float":
			return "float";
		case "Double":
			return "double";
		default:
			return "string";
		}
	}
}
