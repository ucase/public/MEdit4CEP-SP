/*******************************************************************************
 * Copyright (c) 2011, 2019 Juan Boubeta-Puig & David Corral-Plaza
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0 
 * which accompanies this distribution, and is available at 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Juan Boubeta-Puig - initial API and implementation
 *     David Corral-Plaza - modifications
 ******************************************************************************/

package es.uca.modeling.cep.eventpattern.menu.command;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.epsilon.emc.emf.InMemoryEmfModel;
import org.eclipse.epsilon.eol.models.Model;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import domain.CEPDomain;
import domain.DomainFactory;
import domain.DomainPackage;
import domain.diagram.part.DomainDiagramEditorUtil;
import es.uca.modeling.cep.eventpattern.m2t.TransformEventPatternToCode;
import eventpattern.CEPEventPattern;
import eventpattern.ComplexEvent;
import eventpattern.ComplexEventProperty;
import eventpattern.EventPatternElement;
import eventpattern.EventpatternPackage;
import eventpattern.Value;
import eventpattern.diagram.status.EventPatternsStatus;
import eventpattern.impl.EventImpl;
import eventpattern.impl.EventPropertyImpl;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.Response;
import eventpattern.diagram.part.EventpatternDiagramEditor;
import eventpattern.diagram.part.EventpatternDiagramEditorUtil;
import es.uca.modeling.cep.utils.*;

public class SaveValidateEventPatternHandler extends AbstractHandler {

	private Resource globalComplexEventModelResource = null;
	private boolean updatePalettes = false;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();

		// 1� Obtain the active editor's diagram
		EventpatternDiagramEditor patternDiagramEditor = (EventpatternDiagramEditor) HandlerUtil.getActiveEditor(event);

		if (patternDiagramEditor == null || !patternDiagramEditor.getTitle().endsWith("pattern_diagram")) {
			MessageDialog.openError(shell, "Save and Validate Event Pattern", "An event pattern must be open.");
			return null;
		}

		// 2� Save all changes made in the editor
		patternDiagramEditor.doSave(new NullProgressMonitor());

		// 3� Check if there are some problems which must be solved

		IResource ir = (IResource) HandlerUtil.getActiveEditorInput(event).getAdapter(IResource.class);
		IMarker[] problems = null;
		int depth = IResource.DEPTH_INFINITE;

		try {
			problems = ir.findMarkers(IMarker.PROBLEM, true, depth);
		} catch (CoreException e1) {
			e1.printStackTrace();
		}

		if (problems.length > 0) {
			MessageDialog.openError(shell, "Save and Validate Event Pattern",
					"There are some problems that must be solved before saving the event pattern.");

			try {
				HandlerUtil.getActiveWorkbenchWindowChecked(event).getActivePage()
						.showView("org.eclipse.ui.views.ProblemView");
			} catch (PartInitException e) {
				e.printStackTrace();
			}

			return null;
		}

		String domainName = EventPatternsStatus.getDomainName();
		String activePatternName = patternDiagramEditor.getTitle().replace(".pattern_diagram", "");

		IWorkspaceRoot myWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		IProject complexEventProject = myWorkspaceRoot.getProject(domainName + "_complex_events");
		IProject patternProject = myWorkspaceRoot.getProject(domainName + "_patterns");

		DomainPackage.eINSTANCE.eClass();
		// Get the default factory singleton
		DomainFactory factory = DomainFactory.eINSTANCE;

		domain.Event cE = factory.createEvent();

		try {
			if (!complexEventProject.exists()) {
				complexEventProject.create(null);
			}

			// Open if necessary
			if (!complexEventProject.isOpen()) {
				complexEventProject.open(null);
			}

			// Open if necessary
			if (!patternProject.isOpen()) {
				patternProject.open(null);
			}

			IFile complexEventFile = complexEventProject.getFile(domainName + "_complex_events" + ".domain_diagram");

			URI complexEventDiagramUri = URI.createPlatformResourceURI(complexEventProject
					.getFile(domainName + "_complex_events" + ".domain_diagram").getFullPath().toString(), false);
			URI complexEventModelUri = URI.createPlatformResourceURI(
					complexEventProject.getFile(domainName + "_complex_events" + ".domain").getFullPath().toString(),
					false);

			URI activePatternModelUri = URI.createPlatformResourceURI(
					patternProject.getFile(activePatternName + ".pattern").getFullPath().toString(), false);

			URI activePatternDiagramUri = URI.createPlatformResourceURI(
					patternProject.getFile(activePatternName + ".pattern_diagram").getFullPath().toString(), false);

			if (!complexEventFile.exists()) {
				DomainDiagramEditorUtil.createDiagram(complexEventDiagramUri, complexEventModelUri,
						new NullProgressMonitor());
			}

			ResourceSet resourceSet = new ResourceSetImpl();
			Resource complexEventModelResource = resourceSet.getResource(complexEventModelUri, true);
			globalComplexEventModelResource = complexEventModelResource;
			Resource patternModelResource = resourceSet.getResource(activePatternModelUri, true);
			Resource patternDiagramResource = resourceSet.getResource(activePatternDiagramUri, true);

			CEPDomain domainModel = (CEPDomain) complexEventModelResource.getContents().get(0);
			domainModel.setDomainName(domainName);

			for (Iterator iter = EcoreUtil.getAllContents(patternModelResource, true); iter.hasNext();) {
				EObject eObject = (EObject) iter.next();

				if (eObject.getClass().getSimpleName().equals("ComplexEventImpl")) {
					ComplexEvent complexEvent = ((ComplexEvent) eObject);

					cE.setTypeName(complexEvent.getTypeName());
					cE.setImagePath(complexEvent.getImagePath());

					// If the NewComplexEvent is linked with an event, which does not have nested
					// properties
					if (complexEvent.getComplexEventProperties().isEmpty()) {
						for (eventpattern.EventProperty property : ((eventpattern.Event) complexEvent.getInboundLink()
								.get(0).getOperand()).getEventProperties()) {

							domain.EventProperty p = factory.createEventProperty();
							p.setImagePath(property.getImagePath());
							p.setName(property.getName());

							eventpattern.PropertyTypeValue type;
							type = property.getType();

							if (type.compareTo(eventpattern.PropertyTypeValue.STRING) == 0) {
								p.setType(domain.PropertyTypeValue.STRING);
							} else if (type.compareTo(eventpattern.PropertyTypeValue.DOUBLE) == 0) {
								p.setType(domain.PropertyTypeValue.DOUBLE);
							} else if (type.compareTo(eventpattern.PropertyTypeValue.FLOAT) == 0) {
								p.setType(domain.PropertyTypeValue.FLOAT);
							} else if (type.compareTo(eventpattern.PropertyTypeValue.LONG) == 0) {
								p.setType(domain.PropertyTypeValue.LONG);
							} else if (type.compareTo(eventpattern.PropertyTypeValue.INTEGER) == 0) {
								p.setType(domain.PropertyTypeValue.INTEGER);
							} else if (type.compareTo(eventpattern.PropertyTypeValue.BOOLEAN) == 0) {
								p.setType(domain.PropertyTypeValue.BOOLEAN);
							}

							cE.getEventProperties().add(p);
						}
					} else { // If the NewComplexEvent contains complex event properties linked with event
								// properties.
						for (ComplexEventProperty complexProperty : complexEvent.getComplexEventProperties()) {
							domain.EventProperty p = factory.createEventProperty();
							p.setImagePath(complexProperty.getImagePath());
							p.setName(complexProperty.getName());

							// Obtain the type of complex event property

							// complexProperty will only have an inboundLink because it is a unary operator
							String operandName = complexProperty.getInboundLink().get(0).getOperand().getClass()
									.getSimpleName();
							eventpattern.PropertyTypeValue type;

							if (operandName.equals("ValueImpl")) {
								type = ((Value) complexProperty.getInboundLink().get(0).getOperand()).getType();
							} else if (operandName.equals("EventPropertyImpl")) {
								type = ((eventpattern.EventProperty) complexProperty.getInboundLink().get(0)
										.getOperand()).getType();
							} else { // operand of complex event property is an aggregation or arithmetic operator
								type = eventpattern.PropertyTypeValue.DOUBLE;
							}

							if (type.compareTo(eventpattern.PropertyTypeValue.STRING) == 0) {
								p.setType(domain.PropertyTypeValue.STRING);
							} else if (type.compareTo(eventpattern.PropertyTypeValue.DOUBLE) == 0) {
								p.setType(domain.PropertyTypeValue.DOUBLE);
							} else if (type.compareTo(eventpattern.PropertyTypeValue.FLOAT) == 0) {
								p.setType(domain.PropertyTypeValue.FLOAT);
							} else if (type.compareTo(eventpattern.PropertyTypeValue.LONG) == 0) {
								p.setType(domain.PropertyTypeValue.LONG);
							} else if (type.compareTo(eventpattern.PropertyTypeValue.INTEGER) == 0) {
								p.setType(domain.PropertyTypeValue.INTEGER);
							} else if (type.compareTo(eventpattern.PropertyTypeValue.BOOLEAN) == 0) {
								p.setType(domain.PropertyTypeValue.BOOLEAN);
							}

							cE.getEventProperties().add(p);
						}
					}

					break;
				}
			}

			/** Transform Pattern to EPL **/

			activePatternModelUri = URI.createPlatformResourceURI(
					patternProject.getFile(activePatternName + ".pattern").getFullPath().toString(), false);
			resourceSet = new ResourceSetImpl();
			patternModelResource = resourceSet.getResource(activePatternModelUri, true);

			CEPEventPattern eventPatternModel = (CEPEventPattern) patternModelResource.getContents().get(0);

			final Model sourceModel = new InMemoryEmfModel("SourceModel", patternModelResource,
					EventpatternPackage.eINSTANCE);
			sourceModel.setStoredOnDisposal(false);
			sourceModel.setReadOnLoad(true);

			final String patternToEplPath = "/egl/eventpattern-to-epl.egl";

			String result = null;

			try {
				result = TransformEventPatternToCode.executeEGL(sourceModel, eventPatternModel, patternToEplPath);

				if (result != null) {
					EList<EventPatternElement> elements = eventPatternModel.getEventPatternElements();
					ArrayList<String> dependencies = new ArrayList<>();

					for (int i = 0; i < elements.size(); i++) {

						if (elements.get(i).getClass().getSimpleName().equals("EventImpl")) {
							EventImpl e = ((EventImpl) elements.get(i));
							dependencies.add(e.getMongodbId());
							System.out.println("Adding as dependency: " + e.toString() + " ID: " + e.getMongodbId());
						}
					}

					/** Send pattern to API **/
					String patternMongoId = null;

					if (eventPatternModel.getMongodbId() == null) {
						patternMongoId = createPattern(domainName, eventPatternModel.getPatternName(), result,
								dependencies, shell);
					} else {
						// Check if pattern EPL code has changed or not
						String pattern = getPattern(eventPatternModel.getMongodbId(), shell);

						if (!result.replaceAll("\\r|\\n", " ").trim().equalsIgnoreCase(pattern.trim())) {
							// Pattern code has changed, so it has to be updated
							MessageDialog.openInformation(shell, "Modifying Event Pattern",
									"A change in a pattern that is already deployed has been detected. This action will undeploy the "
											+ "previous pattern from the CEP engine. You will have to redeploy it manually. Note also that "
											+ "dependent patterns (if any) will be removed from the API and the CEP engine, but not from MEdit4CEP-SP, "
											+ "so you can edit, save and deploy them again.");

							ArrayList<String> patternsToRemove = removePattern(eventPatternModel.getMongodbId(), shell);

							// Clean affected patterns
							for (int i = 0; i < patternsToRemove.size() - 1; i++) {
								String patternToRemove = patternsToRemove.get(i);
								System.out.println("Cleaning " + patternToRemove);
								URI activePatternModelUriAux = URI.createPlatformResourceURI(
										patternProject.getFile(patternToRemove + ".pattern").getFullPath().toString(),
										false);
								Resource patternModelResourceAux = resourceSet.getResource(activePatternModelUriAux,
										true);
								CEPEventPattern eventPatternModelAux = (CEPEventPattern) patternModelResourceAux
										.getContents().get(0);

								// Set pattern MongoDB ID to null
								eventPatternModelAux.setMongodbId(null);
								ArrayList<EObject> elementsToRemove = new ArrayList<>();
								for (TreeIterator iterator = EcoreUtil.getAllContents(eventPatternModelAux,
										true); iterator.hasNext();) {
									EObject eObjt = (EObject) iterator.next();

									if (eObjt.getClass().getSimpleName().equals("EventImpl")) {
										eventpattern.impl.EventImpl Event = (EventImpl) eObjt;
										if (patternsToRemove.contains(Event.getTypeName())) {
											elementsToRemove.addAll(Event.getOutboundLink());
											elementsToRemove.add(Event);

											// Remove event properties and their links
											for (TreeIterator iterAux = eObjt.eAllContents(); iterAux.hasNext();) {
												EObject eObjectAux = (EObject) iterAux.next();

												if (eObjectAux.getClass().getSimpleName().equals("EventPropertyImpl")) {
													eventpattern.impl.EventPropertyImpl EventProperty = (EventPropertyImpl) eObjectAux;

													elementsToRemove.add(EventProperty);
													elementsToRemove.addAll(EventProperty.getOutboundLink());
												}
											}
										}
									}
								}

								EcoreUtil.removeAll(elementsToRemove);

								try {
									patternModelResourceAux.save(null);
								} catch (Exception e1) {
									System.out.println("Exception: " + e1.getMessage());
								}
							}

							// Clean affected complex events domain diagram
							complexEventProject = myWorkspaceRoot.getProject(domainName + "_complex_events");
							complexEventModelUri = URI.createPlatformResourceURI(complexEventProject
									.getFile(domainName + "_complex_events" + ".domain").getFullPath().toString(),
									false);
							complexEventModelResource = resourceSet.getResource(complexEventModelUri, true);
							domainModel = (CEPDomain) complexEventModelResource.getContents().get(0);

							ArrayList<domain.Event> complexEventsToRemove = new ArrayList<>();
							for (TreeIterator iterator = EcoreUtil.getAllContents(domainModel, true); iterator.hasNext();) {
								EObject eObjct = (EObject) iterator.next();

								if (eObjct.getClass().getSimpleName().equals("EventImpl")) {
									domain.Event domainEvent = (domain.Event) eObjct;
									if (patternsToRemove.contains(domainEvent.getTypeName())) {
										complexEventsToRemove.add(domainEvent);
									}
								}
							}

							EcoreUtil.removeAll(complexEventsToRemove);

							try {
								complexEventModelResource.save(null);
							} catch (IOException e1) {
								e1.printStackTrace();
							}

							// Update affected palettes
							updatePalettes = true;

							// Create pattern again
							patternMongoId = createPattern(domainName, eventPatternModel.getPatternName(), result,
									dependencies, shell);
						}
					}

					if (patternMongoId != null && !patternMongoId.equals("null")) {
						eventPatternModel.setMongodbId(patternMongoId);
						cE.setMongodbId(patternMongoId);
						patternModelResource.save(null);
						globalComplexEventModelResource.save(null);
						MessageDialog.openInformation(shell, "Save and Validate Event Pattern",
								"The pattern has been correctly validated and saved in the API. Please deploy it manually using the options in the menu.");
					}
				} else {
					MessageDialog.openError(shell, "Save and Validate Event Pattern",
							"There was an error generating the pattern code. Please try again");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			boolean simpleEventExists = false;

			for (Iterator iter = EcoreUtil.getAllContents(domainModel.getEvents(), true); iter.hasNext();) {
				EObject eObject = (EObject) iter.next();

				if (eObject.getClass().getSimpleName().equals("EventImpl")) {
					domain.Event simpleEvent = (domain.Event) eObject;

					// If the simple event already exists
					if (simpleEvent.getTypeName().equals(cE.getTypeName())) {
						simpleEventExists = true;
					}
				}
			}

			if (!simpleEventExists) {
				domainModel.getEvents().add(cE);
				complexEventModelResource.save(null);
			}

			try {
				complexEventModelResource.save(null);
			} catch (IOException e1) {
				e1.printStackTrace();
			}

		} catch (CoreException | IOException e1) {
			e1.printStackTrace();
		}

		if (updatePalettes) {
			try {
				IProject domainProject = myWorkspaceRoot.getProject("domain");
				File currentDir = new File(domainProject.getLocationURI());

				// Open if necessary
				if (!patternProject.isOpen()) {
					patternProject.open(null);
				}

				// Change the palette of all existing editors
				currentDir = new File(patternProject.getLocationURI());
				File[] filesAux = currentDir.listFiles();

				for (File file : filesAux) {
					// Refresh simple event tools for all event patterns
					if (!file.isDirectory() && file.getName().matches(".+pattern_diagram")) {

						URI diagramUri = URI.createPlatformResourceURI(
								patternProject.getFile(file.getName()).getFullPath().toString(), false);

						Resource diagramResource = new ResourceSetImpl().getResource(diagramUri, true);

						EventpatternDiagramEditorUtil.openDiagram(diagramResource);

						EventpatternDiagramEditor diagramEditor = (EventpatternDiagramEditor) HandlerUtil
								.getActiveEditor(event);

						System.out.println("Refreshing complex event palette of " + file.getName());
						diagramEditor.refreshPaletteComplexEvent();
					}
				}
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	public String getPattern(String id, Shell shell) {
		String pattern = "";
		Response response = HTTPRequest.GET("/patterns/" + id);

		if (response.code() == 500)
			MessageDialog.openError(shell, "Update Event Pattern",
					"There was an error while retrieving the event pattern from the API. Please try again.");
		else if (response.code() == 404)
			MessageDialog.openError(shell, "Update Event Pattern",
					"The event pattern id was not found in the API. Please check it and try again.");
		else if (response.code() == 200) {
			String jsonString;

			try {
				jsonString = response.body().string();
			} catch (IOException e) {
				e.printStackTrace();
				return "";
			}

			Gson gson = new Gson();
			JsonArray jsonRes = gson.fromJson(jsonString, JsonArray.class);
			response.close();
			return jsonRes.get(0).getAsJsonObject().get("pattern").getAsString();
		}

		response.close();

		return pattern;
	}

	public ArrayList<String> removePattern(String id, Shell shell) {
		Response response = HTTPRequest.DELETE("/patterns/" + id);

		if (response.code() == 500)
			MessageDialog.openError(shell, "Update Event Pattern",
					"There was an error while updating the event pattern in the API. Please try again.");
		else if (response.code() == 404)
			MessageDialog.openError(shell, "Update Event Pattern",
					"The event pattern id was not found in the API. Please check it and try again.");
		else if (response.code() == 200) {
			String jsonString;

			try {
				jsonString = response.body().string();
			} catch (IOException e) {
				e.printStackTrace();
				return new ArrayList<String>();
			}

			Gson gson = new Gson();
			JsonObject jsonRes = gson.fromJson(jsonString, JsonObject.class);
			response.close();
			KafkaCEP.send("streams-undeploy", jsonRes.get("deploymentId").getAsString());
			return new ArrayList<String>(Arrays.asList(jsonRes.get("patternsToRemove").getAsJsonArray().toString()
					.replace("\"", "").replace("[", "").replace("]", "").split(",")));
		}

		response.close();
		return new ArrayList<String>();
	}

	public String createPattern(String domainName, String name, String pattern, ArrayList<String> dependencies,
			Shell shell) {
		String id = "null";

		MediaType JSON = MediaType.parse("application/json; charset=utf-8");

		String ids = "";
		if (dependencies.size() > 0) {
			for (String eId : dependencies)
				ids += "\"" + eId + "\",";

			ids = ids.substring(0, ids.length() - 1);
		}

		String body = "{" + "\"name\": \"" + name + "\"," + "\"pattern\": \""
				+ pattern.replaceAll("\"", "\\\\\"").replaceAll("\\r", " ").replaceAll("\\n", " ") + "\","
				+ "\"timestamp\": \"" + String.valueOf(System.currentTimeMillis()) + "\"," + "\"dependencies\": [" + ids
				+ "]" + "}";

		Response response = HTTPRequest.POST("/patterns", RequestBody.create(JSON, body));

		if (response.code() == 500)
			MessageDialog.openError(shell, "Save and Validate CEP Domain",
					"There was an error while publishing the event pattern in the API. Please try again.");
		else if (response.code() == 404)
			MessageDialog.openError(shell, "Save and Validate CEP Domain",
					"One or more event types that are used in the pattern doesn't exists in the API. Please check it and try again.");
		else if (response.code() == 201) {
			String jsonString;

			try {
				jsonString = response.body().string();
			} catch (IOException e) {
				e.printStackTrace();
				return id;
			}

			Gson gson = new Gson();
			JsonObject jsonRes = gson.fromJson(jsonString, JsonObject.class);
			response.close();
			id = jsonRes.get("id").getAsString();
		}

		response.close();

		return id;
	}
}
