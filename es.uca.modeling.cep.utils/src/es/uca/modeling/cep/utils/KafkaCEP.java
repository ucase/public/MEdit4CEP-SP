package es.uca.modeling.cep.utils;

import java.util.ArrayList;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.google.gson.JsonArray;

public class KafkaCEP {

	private static Producer<Long, String> kafkaProducer = null;

	public static void initialize() {
		Properties props = new Properties();
		
		if(!MEdit4CEPConstants.isCustomizated()) {
			Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
			
			ConstantsDialog dialog = new ConstantsDialog(shell);
			dialog.create();
			dialog.open();
		}
		
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, MEdit4CEPConstants.getKafkaHost());
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "MEdit4CEP-SP-Producer");
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		kafkaProducer = new KafkaProducer<>(props);
	}

	public static boolean send(String topic, String message) {
		if (message.equalsIgnoreCase("") || message.equalsIgnoreCase("null") || message.equalsIgnoreCase(null))
			return true;
		
		if (kafkaProducer == null)
			initialize();

		System.out.println("Sending to " + topic + " the message: " + message);

		try {
			ProducerRecord<Long, String> record = new ProducerRecord<>(topic, message);
			kafkaProducer.send(record);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static boolean send(String topic, JsonArray message) {
		if (message.size() == 1 && !message.isJsonNull())
			return send(topic, message.getAsString());
		else if (message.size() > 1) {
			ArrayList<String> ids = new ArrayList<>();
			for (int i = 0; i < message.size(); i++)
				ids.add(message.get(i).getAsString());
			return send(topic, String.join(",", ids));
		} else
			return true;
	}

}
