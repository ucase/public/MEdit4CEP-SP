/*******************************************************************************
 * Copyright (c) 2011, 2019 Juan Boubeta-Puig
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0 
 * which accompanies this distribution, and is available at 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Juan Boubeta-Puig - initial API and implementation
 *     David Corral-Plaza - modifications
 ******************************************************************************/

package es.uca.modeling.cep.utils;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.regex.Pattern;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class ConstantsDialog extends TitleAreaDialog {

	private Text APIHostText;
	private Text APIKeyText;
	private Text kafkaHostText;

	private String APIHost;
	private String APIKey;
	private String kafkaHost;

	public ConstantsDialog(Shell parentShell) {
		super(parentShell);
	}

	@Override
	public void create() {
		super.create();
		setTitle("MEdit4CEP Constants");
		setMessage("Please insert the following information to communicate with the API and Kafka Server:",
				IMessageProvider.INFORMATION);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));
		GridLayout layout = new GridLayout(2, false);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		container.setLayout(layout);

		createFields(container);

		return area;
	}

	private void createFields(Composite container) {
		Label APIHostLabel = new Label(container, SWT.NONE);
		APIHostLabel.setText("API Host");

		GridData APIHostData = new GridData();
		APIHostData.grabExcessHorizontalSpace = true;
		APIHostData.horizontalAlignment = GridData.FILL;

		APIHostText = new Text(container, SWT.BORDER);
		APIHostText.setLayoutData(APIHostData);
		APIHost = MEdit4CEPConstants.getAPIHost();
		APIHostText.setText(MEdit4CEPConstants.getAPIHost());

		Label APIKeyLabel = new Label(container, SWT.NONE);
		APIKeyLabel.setText("API Key");

		GridData APIKeyData = new GridData();
		APIKeyData.grabExcessHorizontalSpace = true;
		APIKeyData.horizontalAlignment = GridData.FILL;

		APIKeyText = new Text(container, SWT.BORDER);
		APIKeyText.setLayoutData(APIHostData);
		APIKey = MEdit4CEPConstants.getAPIHost();
		APIKeyText.setText(MEdit4CEPConstants.getAPIKey());

		Label kafkaHostLabel = new Label(container, SWT.NONE);
		kafkaHostLabel.setText("Kafka Host");

		GridData kafkaHostData = new GridData();
		kafkaHostData.grabExcessHorizontalSpace = true;
		kafkaHostData.horizontalAlignment = GridData.FILL;

		kafkaHostText = new Text(container, SWT.BORDER);
		kafkaHostText.setLayoutData(kafkaHostData);
		kafkaHost = MEdit4CEPConstants.getKafkaHost();
		kafkaHostText.setText(MEdit4CEPConstants.getKafkaHost());
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	private void saveInput() {
		APIHost = APIHostText.getText();
		MEdit4CEPConstants.setAPIHost(APIHost);
		APIKey = APIKeyText.getText();
		MEdit4CEPConstants.setAPIKey(APIKey);
		kafkaHost = kafkaHostText.getText();
		MEdit4CEPConstants.setKafkaHost(kafkaHost);
		MEdit4CEPConstants.setCustomizated(true);
	}

	@Override
	protected void okPressed() {
		boolean error = false;

		try {
			URL apiURL = new URL(APIHostText.getText());
			apiURL.toURI();
		} catch (MalformedURLException | URISyntaxException e) {
			setMessage("API Host fail: The API Host must be a valid URL, i.e., http://localhost:8080/api/v1", IMessageProvider.ERROR);
			error = true;
		}

		if (APIKeyText.getText().equals("")) {
			setMessage("API Key fail: Please enter a valid API Key.", IMessageProvider.ERROR);
			error = true;
		}

		Pattern p = Pattern.compile("^" + "(((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,6}" // Domain name
				+ "|" + "localhost" // localhost
				+ "|" + "(([0-9]{1,3}\\.){3})[0-9]{1,3})" // Ip
				+ ":" + "[0-9]{1,5}$"); // Port

		if (!p.matcher(kafkaHostText.getText()).matches()) {
			setMessage("Kafka Host fail: The Kafka Host must be composed by 'host:port', such as localhost:9092 or 192.168.0.125:9092", IMessageProvider.ERROR);
			error = true;
		}

		if (!error) {
			saveInput();
			super.okPressed();
		}
	}
}