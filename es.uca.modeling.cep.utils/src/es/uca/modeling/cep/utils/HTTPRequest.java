package es.uca.modeling.cep.utils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HTTPRequest {

	public static OkHttpClient client;
	

	static {
		client = new OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS).writeTimeout(30, TimeUnit.SECONDS)
				.readTimeout(30, TimeUnit.SECONDS).build();
		
		if(!MEdit4CEPConstants.isCustomizated()) {
		    Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
	
			ConstantsDialog dialog = new ConstantsDialog(shell);
			dialog.create();
			dialog.open();
		}
	}

	public static Response GET(String endpoint) {
		Request request = new Request.Builder().addHeader("Authorization", "Bearer " + MEdit4CEPConstants.getAPIKey())
				.url(MEdit4CEPConstants.getAPIHost() + endpoint).build();

		return execute(request);
	}

	public static Response DELETE(String endpoint) {
		Request request = new Request.Builder().addHeader("Authorization", "Bearer " + MEdit4CEPConstants.getAPIKey())
				.url(MEdit4CEPConstants.getAPIHost() + endpoint).delete().build();

		return execute(request);
	}

	public static Response POST(String endpoint, RequestBody body) {
		Request request = new Request.Builder().addHeader("Authorization", "Bearer " + MEdit4CEPConstants.getAPIKey())
				.url(MEdit4CEPConstants.getAPIHost() + endpoint).post(body).build();

		return execute(request);
	}

	public static Response UPDATE(String endpoint, RequestBody body) {
		Request request = new Request.Builder().addHeader("Authorization", "Bearer " + MEdit4CEPConstants.getAPIKey())
				.url(MEdit4CEPConstants.getAPIHost() + endpoint).put(RequestBody.create(null, "")).build();

		return execute(request);
	}

	public static Response execute(Request request) {
		Call call = client.newCall(request);
		Response response = null;

		try {
			response = call.execute();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return response;
	}
}
