package es.uca.modeling.cep.utils;

public class MEdit4CEPConstants {
	private static String APIHost = "http://localhost:8080/api/v1";
	private static String APIKey = "@Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2";
	private static String kafkaHost = "localhost:9092";
	private static String kafkaTopic = "streams-epl";
	private static boolean customizated = false;

	public static void setAPIHost(String newAPIHost) {
		APIHost = newAPIHost;
	}
	
	public static void setAPIKey(String newAPIKey) {
		APIKey = newAPIKey;
	}

	public static void setKafkaHost(String newKafkaHost) {
		kafkaHost = newKafkaHost;
	}

	public static void setKafkaTopic(String newKafkaTopic) {
		kafkaTopic = newKafkaTopic;
	}
	
	public static void setCustomizated(boolean newCustomizated) {
		customizated = newCustomizated;
	}

	public static String getAPIHost() {
		return APIHost;
	}
	
	public static String getAPIKey() {
		return APIKey;
	}

	public static String getKafkaHost() {
		return kafkaHost;
	}

	public static String getKafkaTopic() {
		return kafkaTopic;
	}

	public static boolean isCustomizated() {
		return customizated;
	}
}
